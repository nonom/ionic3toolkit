Ionic 3 Natives Professional Edition 1.2 README
================================================

Thank you for downloading Ionic 3 Natives as part of Ionic 3 Toolkit Professional Edition distribution.

Ionic 3 Natives distribution includes the source code of a full working Cordova/Ionic 
project. It's ready to run (almost) out the box.

Overview
--------

Specifically, your pack consists of the following two 
directories:

    * [project]
      Includes an Ionic/Cordova project. This is the project
      that can be used for development.
    * [license]
      Includes the License information and references to
      third-party licenses.

You can use the ionic project to build a final mobile application or
to create your own ionic project for any platform, as long as this
is permitted by the License. You can dig into the source psd files and
modify them the way you want and reuse, as long as this is permitted by
the License.

Brief installation guide
------------------------
Quick start guide
https://docs.google.com/document/d/1qol3rel_KP8lgkSqMrXMlvpvWdP-Al1OBnL4Br-Vm4k/edit?usp=sharing

Problems/Questions
------------------
If you experience any problem, please post a message to our Support Centre below and submit your question choosing the product your enquiry refers to:
http://appseed.desk.com/customer/portal/emails/new

Happy hacking and thank you for using our product!

The AppSeed team
