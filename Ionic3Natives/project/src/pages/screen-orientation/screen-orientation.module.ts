import { NgModule } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { IonicModule } from 'ionic-angular';

import { ScreenOrientationPage } from './screen-orientation.page';

@NgModule({
	imports: [IonicModule],
	declarations: [ScreenOrientationPage],
	entryComponents: [ScreenOrientationPage],
	providers: [ScreenOrientation]
})
export class ScreenOrientationModule {

}