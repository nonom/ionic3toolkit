import { NgModule } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { IonicModule } from 'ionic-angular';

import { LocalNotificationsPage } from './local-notifications.page';

@NgModule({
	imports: [IonicModule],
	declarations: [LocalNotificationsPage],
	entryComponents: [LocalNotificationsPage],
	providers: [LocalNotifications]
})
export class LocalNotificationsModule {

}