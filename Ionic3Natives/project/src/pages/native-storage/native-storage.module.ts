import { NgModule } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicModule } from 'ionic-angular';

import { NativeStorageEditPage } from './edit/native-storage.edit.page';
import { NativeStorageListPage } from './list/native-storage.list.page';

@NgModule({
	imports: [IonicModule],
	declarations: [
		NativeStorageEditPage,
		NativeStorageListPage
	],
	entryComponents: [
		NativeStorageEditPage,
		NativeStorageListPage
	],
	providers: [NativeStorage]
})
export class NativeStorageModule {

}