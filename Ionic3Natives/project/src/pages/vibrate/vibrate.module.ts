import { NgModule } from '@angular/core';
import { Vibration } from '@ionic-native/vibration';
import { IonicModule } from 'ionic-angular';

import { VibratePage } from './vibrate.page';

@NgModule({
	imports: [IonicModule],
	declarations: [VibratePage],
	entryComponents: [VibratePage],
	providers: [Vibration]
})
export class VibrateModule {

}