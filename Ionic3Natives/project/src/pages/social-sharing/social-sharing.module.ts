import { NgModule } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicModule } from 'ionic-angular';

import { SocialSharingPage } from './social-sharing.page';

@NgModule({
	imports: [IonicModule],
	declarations: [SocialSharingPage],
	entryComponents: [SocialSharingPage],
	providers: [SocialSharing]
})
export class SocialSharingModule {

}