import { NgModule } from '@angular/core';
import { Insomnia } from '@ionic-native/insomnia';
import { IonicModule } from 'ionic-angular';

import { InsomniaPage } from './insomnia.page';

@NgModule({
	imports: [IonicModule],
	declarations: [InsomniaPage],
	entryComponents: [InsomniaPage],
	providers: [Insomnia]
})
export class InsomniaModule {

}