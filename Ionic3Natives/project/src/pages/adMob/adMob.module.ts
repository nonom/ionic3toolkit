import { NgModule } from '@angular/core';
import { AdMobPro } from '@ionic-native/admob-pro';
import { IonicModule } from 'ionic-angular';

import { AdMobPage } from './adMob.page';

@NgModule({
	imports: [IonicModule],
	declarations: [AdMobPage],
	entryComponents: [AdMobPage],
	providers: [AdMobPro]
})
export class AdMobModule {

}