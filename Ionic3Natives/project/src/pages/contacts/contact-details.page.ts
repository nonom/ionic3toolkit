import { Component } from '@angular/core';
import { Contact } from '@ionic-native/contacts';
import { NavParams } from 'ionic-angular';

@Component({
	templateUrl: 'contact-details.html'
})
export class ContactDetailsPage {
	contact: Contact;

	constructor(navParams: NavParams) {
		this.contact = <Contact>navParams.get('contact');
		console.log(this.contact);
	}
}
