import { Component } from '@angular/core';
import { ContactField, ContactFieldType, ContactName, Contacts } from '@ionic-native/contacts';
import { ModalController, NavController } from 'ionic-angular';
import { isCordovaAvailable } from '../../services/is-cordova-available';
import { AddContactPage } from './add-contact.page';
import { Contact } from './contact';
import { ContactDetailsPage } from './contact-details.page';

@Component({
	templateUrl: 'contacts.html'
})
export class ContactsPage {
	contacts: Contact[] = [];
	contactInfo: ContactFieldType = 'name';
	searchFilter: string;

	private nav: NavController;
	private modalController: ModalController;

	constructor(nav: NavController, modalController: ModalController, private cntcs: Contacts) {
		this.nav = nav;
		this.modalController = modalController;
	}

	addContact() {
		let modal = this.modalController.create(AddContactPage);
		modal.onDidDismiss(newContact => {
			if (!newContact) {
				return;
			}

			var contact = this.cntcs.create();
			contact.name = new ContactName(null, newContact.lastName, newContact.firstName);
			contact.phoneNumbers = [new ContactField('mobile', newContact.phoneNumber)];

			contact.save().then(() => {
				alert('Saved');
			}, (error) => {
				alert(error);
			});
		});
		modal.present();
	}

	findContacts() {
		if (!isCordovaAvailable()) {
			return false;
		}

		let options = {
			filter: this.searchFilter,
			multiple: true
		};
		this.contacts = [];

		this.cntcs.find([this.contactInfo], options).then(response => {
			this.contacts = response;
		});
	}

	showDetails(contact: Contact) {
		console.log(contact);
		this.nav.push(ContactDetailsPage, {
			contact: contact
		});
	}
}
