import { NgModule } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicModule } from 'ionic-angular';
import { PositionPage } from './position.page';

@NgModule({
	imports: [IonicModule],
	declarations: [PositionPage],
	entryComponents: [PositionPage],
	providers: [Geolocation]
})
export class PositionModule {

}