import { NgModule } from '@angular/core';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { IonicModule } from 'ionic-angular';
import { LaunchNavigatorPage } from './launch-navigator.page';

@NgModule({
	declarations: [LaunchNavigatorPage],
	entryComponents: [LaunchNavigatorPage],
	imports: [IonicModule],
	providers: [LaunchNavigator]
})
export class LaunchNavigatorModule {

}