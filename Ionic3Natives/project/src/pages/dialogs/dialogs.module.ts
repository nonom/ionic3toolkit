import { NgModule } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs';
import { IonicModule } from 'ionic-angular';

import { DialogsPage } from './dialogs.page';

@NgModule({
	imports: [IonicModule],
	declarations: [DialogsPage],
	entryComponents: [DialogsPage],
	providers: [Dialogs]
})
export class DialogsModule {

}