import { NgModule } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { IonicModule } from 'ionic-angular';

import { PhotoViewerPage } from './photo-viewer.page';

@NgModule({
	imports: [IonicModule],
	declarations: [PhotoViewerPage],
	entryComponents: [PhotoViewerPage],
	providers: [PhotoViewer, Camera]
})
export class PhotoViewerModule {

}