import { NgModule } from '@angular/core';
import { Network } from '@ionic-native/network';
import { IonicModule } from 'ionic-angular';

import { NetworkPage } from './network.page';

@NgModule({
	imports: [IonicModule],
	declarations: [NetworkPage],
	entryComponents: [NetworkPage],
	providers: [Network]
})
export class NetworkModule {

}