import { NgModule } from '@angular/core';
import { Sim } from '@ionic-native/sim';
import { IonicModule } from 'ionic-angular';

import { SimPage } from './sim.page';

@NgModule({
	imports: [IonicModule],
	declarations: [SimPage],
	entryComponents: [SimPage],
	providers: [Sim]
})
export class SimModule {

}