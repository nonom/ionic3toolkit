import { NgModule } from '@angular/core';
import { Shake } from '@ionic-native/shake';
import { IonicModule } from 'ionic-angular';

import { ShakePage } from './shake.page';

@NgModule({
	imports: [IonicModule],
	declarations: [ShakePage],
	entryComponents: [ShakePage],
	providers: [Shake]
})
export class ShakeModule {

}