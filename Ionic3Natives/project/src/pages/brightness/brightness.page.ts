import { Component } from '@angular/core';
import { isCordovaAvailable } from '../../services/is-cordova-available';
import { Brightness } from '@ionic-native/brightness';

@Component({
	templateUrl: 'brightness.html'
})
export class BrightnessPage {
	brightness: number;
	isCordova: boolean = true;

	constructor(private bright: Brightness) {
		if (!isCordovaAvailable()) {
			this.isCordova = false;
		}
	}

	onChange(newValue) {
		this.bright.setBrightness(newValue / 100);
	}
}
